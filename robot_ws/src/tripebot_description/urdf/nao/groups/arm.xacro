<?xml version="1.0" ?>
<robot xmlns:xacro="http://ros.org/wiki/xacro">

  <xacro:include filename="$(find tripebot_description)/urdf/nao/parts/arm_links.xacro"/>
  <xacro:include filename="$(find tripebot_description)/urdf/nao/parts/hand_links.xacro"/>

  <xacro:include filename="$(find tripebot_description)/urdf/nao/transmissions/arm_transmissions.xacro"/>
  <xacro:include filename="$(find tripebot_description)/urdf/nao/plugins/mimic_plugins.gazebo.xacro"/>    


  <xacro:macro name="arm" params="side parent_link transmission_type:=|VelocityJointInterface">

    <xacro:property name="reflect"/>
    <xacro:if value="${side == 'left'}"> <xacro:property name="reflect" value="1"/> </xacro:if>
    <xacro:if value="${side == 'right'}"> <xacro:property name="reflect" value="-1"/> </xacro:if>

    <!-- Arm Links -->

    <xacro:arm_links side="${side}"/>
    <xacro:hand_links side="${side}"/>

    <!-- Arm Joints -->

    <joint name="${side}_shoulder_pitch_joint" type="revolute">
      <parent link="${parent_link}"/>
      <child link="${side}_shoulder_pitch_link"/>
      <origin rpy="0 0 0" xyz="0 ${reflect*0.098} 0.1"/>
      <axis xyz="0 1.0 0"/>      
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="1.329"
             lower="-2.08567"
             upper="2.08567"
             velocity="8.26797"/>
      
    </joint>

    <xacro:property name="offset_shoulder" value="0.0"/>
    <xacro:if value="${side == 'right'}"> 
      <xacro:property name="offset_shoulder" value="1.012291"/> 
    </xacro:if>

    <joint name="${side}_shoulder_roll_joint" type="revolute">
      <parent link="${side}_shoulder_pitch_link"/>
      <child link="${side}_shoulder_roll_link"/>
      <origin rpy="0 0 0" xyz="0 0 0"/>
      <axis xyz="0 0 1.0"/>      
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="1.7835" 
             lower="${-0.314159-offset_shoulder}" 
             upper="${1.32645-offset_shoulder}" 
             velocity="7.19407"/>
    </joint>

    <joint name="${side}_elbow_yaw_joint" type="revolute">
      <parent link="${side}_shoulder_roll_link"/>
      <child link="${side}_elbow_yaw_link"/>
      <origin rpy="0 0 0" xyz="0.105 ${reflect*0.015} 0"/>
      <axis xyz="1.0 0 0"/>
      <dynamics damping="0.1" friction="1000.0"/>      
      <limit effort="1.547" 
             lower="-2.08567" 
             upper="2.08567" 
             velocity="8.26797"/>
    </joint>

    <xacro:property name="offset_elbow" value="0.0"/>
    <xacro:if value="${side == 'right'}"> 
      <xacro:property name="offset_elbow" value="1.5795266"/> 
    </xacro:if>

    <joint name="${side}_elbow_roll_joint" type="revolute">
      <parent link="${side}_elbow_yaw_link"/>
      <child link="${side}_elbow_roll_link"/>
      <origin rpy="0 0 0" xyz="0 0 0"/>
      <axis xyz="0 0 1.0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="1.532" 
             lower="${-1.54462+offset_elbow}" 
             upper="${-0.0349066+offset_elbow}"
             velocity="7.19407"/>
    </joint>

    <!-- Palm joints -->

    <joint name="${side}_wrist_yaw_joint" type="revolute">
      <parent link="${side}_elbow_roll_link"/>
      <child link="${side}_wrist_yaw_link"/>
      <origin rpy="0 0 0" xyz="0.05595 0 0"/>
      <axis xyz="1.0 0 0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.4075"
             lower="-1.82387"
             upper="1.82387"
             velocity="24.6229"/>
    </joint>
    
    <joint name="${side}_hand_joint" type="revolute">
      <parent link="${side}_wrist_yaw_link"/>
      <child link="${side}_hand_link"/>
      <origin rpy="0 0 0" xyz="0.05775 0 -0.01213"/>
      <axis xyz="1.0 0 0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0"
             upper="1.0"
             velocity="8.33"/>
    </joint>

    <!-- Finger Joints -->

    <joint name="${side}_finger11_joint" type="revolute">
      <parent link="${side}_wrist_yaw_link"/>
      <child link="${side}_finger11"/>
      <origin rpy="1.8862 0.976662 0.264067" xyz="0.06907 0.01157 -0.00304"/>      
      <axis xyz="0 0 1.0"/>
      <mimic joint="${side}_hand_joint" multiplier="0.999899" offset="0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0"
             upper="1.0"
             velocity="8.33"/>
    </joint>

    <joint name="${side}_finger12_joint" type="revolute">
      <parent link="${side}_finger11"/>
      <child link="${side}_finger12"/>
      <origin rpy="0 0 -0.999899" xyz="0.01436 0 0"/>
      <axis xyz="0 0 1.0"/>
      <mimic joint="${side}_hand_joint" multiplier="0.999899" offset="0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0"
             upper="1.0"
             velocity="8.33"/>
    </joint>

    <joint name="${side}_finger13_joint" type="revolute">
      <parent link="${side}_finger12"/>
      <child link="${side}_finger13"/>
      <origin rpy="0 0 -0.999899" xyz="0.01436 0 0"/>
      <axis xyz="0 0 1.0"/>
      <mimic joint="${side}_hand_joint" multiplier="0.999899" offset="0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0"
             upper="1.0"
             velocity="8.33"/>
    </joint>

    <joint name="${side}_finger21_joint" type="revolute">
      <parent link="${side}_wrist_yaw_link"/>
      <child link="${side}_finger21"/>
      <origin rpy="1.25539 0.976662 -0.264067" xyz="0.06907 -0.01157 -0.00304"/>
      <axis xyz="0 0 1.0"/>
      <mimic joint="${side}_hand_joint" multiplier="0.999899" offset="0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0"
             upper="1.0"
             velocity="8.33"/>
    </joint>

    <joint name="${side}_finger22_joint" type="revolute">
      <parent link="${side}_finger21"/>
      <child link="${side}_finger22"/>
      <origin rpy="0 0 -0.999899" xyz="0.01436 0 0"/>
      <axis xyz="0 0 1.0"/>
      <mimic joint="${side}_hand_joint" multiplier="0.999899" offset="0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0"
             upper="1.0"
             velocity="8.33"/>
    </joint>

    <joint name="${side}_finger23_joint" type="revolute">
      <parent link="${side}_finger22"/>
      <child link="${side}_finger23"/>
      <origin rpy="0 0 -0.999899" xyz="0.01436 0 0"/>
      <axis xyz="0 0 1.0"/>
      <mimic joint="${side}_hand_joint" multiplier="0.999899" offset="0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0"
             upper="1.0"
             velocity="8.33"/>
    </joint>

    <joint name="${side}_thumb1_joint" type="revolute">
      <parent link="${side}_wrist_yaw_link"/>
      <child link="${side}_thumb1"/>
      <origin rpy="-1.5708 0.0472984 -3.26826e-08" xyz="0.04895 0 -0.02638"/>
      <axis xyz="0 0 1.0"/>
      <mimic joint="${side}_hand_joint" multiplier="0.999899" offset="0.7"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0.7"
             upper="1.7"
             velocity="8.33"/>
    </joint>

     <joint name="${side}_thumb2_joint" type="revolute">
      <parent link="${side}_thumb1"/>
      <child link="${side}_thumb2"/>
      <origin rpy="0 0 -0.999899" xyz="0.00736 -0.00736 0"/>
      <axis xyz="0 0 1.0"/>
      <mimic joint="${side}_hand_joint" multiplier="0.999899" offset="0"/>
      <dynamics damping="0.1" friction="1000.0"/>
      <limit effort="0.292"
             lower="0"
             upper="1.0"
             velocity="8.33"/>
    </joint>
    
    <!-- Arm Transmissions -->
    <xacro:arm_transmissions side="${side}" transmission_type="${transmission_type}"/> 

    <!-- Mimic Joints Plugins -->

    <xacro:mimic_joint mimicked_joint="${side}_hand_joint" link_name="${side}_finger11"/>
    <xacro:mimic_joint mimicked_joint="${side}_hand_joint" link_name="${side}_finger12"/>
    <xacro:mimic_joint mimicked_joint="${side}_hand_joint" link_name="${side}_finger13"/>
    <xacro:mimic_joint mimicked_joint="${side}_hand_joint" link_name="${side}_finger21"/>
    <xacro:mimic_joint mimicked_joint="${side}_hand_joint" link_name="${side}_finger22"/>
    <xacro:mimic_joint mimicked_joint="${side}_hand_joint" link_name="${side}_finger23"/>
    <xacro:mimic_joint mimicked_joint="${side}_hand_joint" link_name="${side}_thumb1"/>
    <xacro:mimic_joint mimicked_joint="${side}_hand_joint" link_name="${side}_thumb2"/>

  </xacro:macro>

</robot>