#! /bin/bash

source $(dirname $BASH_SOURCE)/../config

TRIPEBOT_PATH=$(dirname $BASH_SOURCE)/../..
TRIPEBOT_PATH=$(realpath $TRIPEBOT_PATH)

HOST_USER=$(whoami)
INNER_HOSTNAME=$HOST_USER-docker

if [ "$USE_NVIDIA_DOCKER" = true ]; then
  if [ -z "$(which nvidia-docker)" ]; then 
    echo "nvidia docker not found in the system"
    exit 0
  fi

  DOCKER_RUNNER=nvidia-docker
else
  DOCKER_RUNNER=docker
fi